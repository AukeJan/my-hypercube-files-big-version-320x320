**BUILD LOG**

This project was started on 2020-05-18.

First, here is a screenshot and a render of the proposed machine without the enclosure panels 
(they would simply hide everything).

These images will be replaced from time to time as random bits of the project change or if I decide to screw around with my rendering settings :smiley: ).

The printer stands 540mm tall (plus the green/black feet) and should have a ~320 x 320 x 300 mm build volume.

X uses 10 mm OD/8 mm ID carbon fibre tubes, Y uses 8 mm chromed steel rods, and Z uses 12 mm chromed steel rods.

I'm using Misumi Series-5 2020, 2040, and 4040 extrusions; that which is depicted here was derived from the technical drawings for those products.

Colors of the printed parts are shown as intended, not necessarily what I will print them with (i.e. if I run out of some color).

Newest entries in this log will be at the bottom.

![](Images/example-screenshot.png)
![](Images/Render.png)

The base machine:  https://www.thingiverse.com/thing:1752766

I'll be adding/using the following mods:

* This machine is designed with an integrated enclosure, which is why I went with a mixture of 2020, 2040, and 4040 -- the enclosure panels just slide into the extrusions' slots.  If not for the enclosure, it could have been made entirely of 2020.
* The piece of 2020 going across under the bed provides a place for the power supplies' middle-facing sides to attach to, and for the front of the bed to rest on when it's parked at the bottom of the print volume.
* A simple "pad" and two spacers (made by me) to attach to the crossbar and rear, lower X bar, so that the bed frame will sit level when parked, and won't make metal-on-metal contact on the crossbar.
* Stronger 8mm Y rod mounts: https://www.thingiverse.com/thing:2169166 
* XY motor angle adjusters: https://www.thingiverse.com/thing:2036245
* PSU mounting bracket: https://www.thingiverse.com/thing:2467824 (but significantly reduced in size, plus a modified version to attach the right side of the PSU to the lower front frame piece)
* Bed mounting brackets:  https://www.thingiverse.com/thing:2471033 (plus some standoffs since I don't use bed springs)
* Corner brackets for the middle/back of the bed carriage:  https://www.thingiverse.com/thing:2740948 (modified by me to be much thicker)
* X carriage:  https://www.thingiverse.com/thing:2374167  (further modified by me to accept dual LM10UU bushings on top rather than an LM12LUU, and to alter the endstop mount position/angle)
* Z bearing mounts:  https://www.thingiverse.com/thing:2392973 (because I want to use regular LM12UU bearings, not long LM12LUU)
* The nameplate:  https://www.thingiverse.com/thing:2087379
* The pink LCD case at the top right and the green and white controller case at the rear, lower right are by me, and are derived from my generic BigTreeTech SKR v1.x/RRD 12864 case:  https://www.prusaprinters.org/prints/7686-case-enclosure-for-skr-and-rrd-12864-display
* The extruder is my Geared B'struder, https://www.prusaprinters.org/prints/7456-gregs-bwadestruder-geared-bstruder-bowden-extruder 
* The power switch mount is a customized version of the one I made for LACK table enclosures, https://www.prusaprinters.org/prints/7478-lack-table-power-switch-mount (incorporating a rocker switch for the mains, and a push-on-push-off switch for the lighting)
* The 24v PSU cover is a my own design, with a hole for the mains wire and an inset female XT90 connector for the 24v side.
* The mains socket mount on the back of the machine is something I threw together just for this machine.
* Ditto for the spool holder.
* I've also added a mount for a 688ZZ bearing at the top of the lead screw, as a way of protecting it from lateral movement, since it's so long.
* Belt tensioner: https://www.thingiverse.com/thing:2396839 (modified by me to be as low-profile as possible)
* not shown in the render are printed slot covers I intend to add, and whatever bits I'll need to come up with for cable management.

**2020-05-18**

Built the frame using the same end-tapping method as in my smaller machine.  Inches tape measure on top for scale (sorry, I don't have a proper metric one).  :stuck_out_tongue: 

![](Images/P1180605.JPG)

**2020-05-19**

Installed the feet and lower enclosure panel.  In hindsight, I probably should have mounted it differently; I forgot to account for "overlap" of the corners of the panel with the 4040.  Little cuts in the corners fixed that, but it also meant taking the bottom-front of the frame apart to get the panel in.  The others will just slide in from the top (which is why the top back and sides are made from doubled-up 2020).

![](Images/P1180606.JPG)

**2020-05-21**

Installed the Y and Z rods.

![](Images/P1180607.JPG)
![](Images/P1180609.JPG)

**2020-05-22**

Installed the X gantry and carriage, Z bearing mounts and bed frame, and the front bedrest/crossbar (not pictured).

![](Images/P1180611.JPG)
![](Images/P1180612.JPG)

Also installed the XY motors, motor sag adjusters, and idler pulleys.

![](Images/P1180613-14.JPG)
