This is my attempt to model and build a bigger Hypercube than the one I have now (which works great). After all is said and done, it will have a maximum build volume of 320x320x300 mm (with a movement range of about 330x325 mm on X/Y).

This repository contains all of the original Hypercube parts.

It also includes a bunch of parts I downloaded from Thingiverse, modified here and there as needed.  Most of them are listed in the build log.

Many of the items referenced in the .blend are pulled from my "Standard Library" Blender file, many of which in turn came from various sources. See https://www.prusaprinters.org/prints/16640-my-blender-3d-printing-partsmaterials-library for that library file.

Finally, all of my customized parts are included, some tools/jigs, and a few other things.

You can find the full build log [HERE](BUILD LOG.md).

The list of printed parts is [HERE](PRINTED PARTS.md).
